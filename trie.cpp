#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())
#define pii pair
#define MP make_pair

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2 scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)

struct trie
{
    trie *child[26];
    int leaf;
    trie()
    {
        fo(i,0,26)child[i]=NULL;
        leaf=0;
    }
};

void update(struct trie* root,const char *s)
{
    struct trie *ptr=root;
    int len=strlen(s);
    fo(i,0,len)
    {
        int idx=(int)(s[i]-'a');
        if(!ptr->child[idx])
        {
            ptr->child[idx]= new trie();
        }
        ptr=ptr->child[idx];
    }
    ptr->leaf=1;
}


bool src(struct trie* root,const char *key)
{
    struct trie *ptr=root;
    int len=strlen(key);
    fo(i,0,len)
    {
        int idx=(int)(key[i]-'a');
        if(!ptr->child[idx] )return 0;
        ptr=ptr->child[idx];
    }
    return ptr->leaf;
}

void trie_del(trie *ptr)
{
    fo(i,0,26)
    {
        if(ptr->child[i])
        {
            trie_del(ptr->child[i]);
            delete ptr->child[i];
        }
    }
}



char s[100];

int main()
{
    int n;
    read(n);
    trie *root=new trie();

    fo(i,0,n)
    {
        scanf("%s",s);
        update(root,s);
    }

    int m;
    read(m);
    fo(i,0,m){
        scanf("%s",s);
        puts( src(root,s) ? "YES":"NO");
    }

    trie_del(root);
    delete root;

    return 0;
}














