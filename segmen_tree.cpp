//segment tree for RMV...
//Ashini Singha
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int tree[N*4];

//init or update...
void init(int node,int be,int ed,int pos,int val)
{

    if(be==ed && be==pos)
    {
        tree[node]+=val;
        return;
    }

    int mid=(be+ed)>>1;
    if(pos<=mid)init(node*2,be,mid,pos,val);
    else init(node*2+1,mid+1,ed,pos,val);
    tree[node]=max(tree[node*2],tree[node*2+1]);
}

int query(int node,int be,int ed,int i,int j)
{
    if(i==be && j==ed)
    {
        return tree[node];
    }
    int mid=(be+ed)>>1;
    //be....i.....j.....mid.......ed
    if(j<=mid) return query(node*2,be,mid,i,j);
    //be......mid...i....j....ed
    else if(i>mid) return query(node*2+1,mid+1,ed,i,j);
    else
    {
        // be.....i....mid.....j......ed
        int ret1=query(node*2,be,mid,i,mid);
        int ret2=query(node*2+1,mid+1,ed,mid+1,j);
        return max(ret1,ret2);
    }
}

int main()
{
    int n;//no. of elements
    scanf("%d",&n);
    for(int i=1; i<=n; i++)
    {
        int x; //value
        scanf("%d",&x);
        init(1,1,n,i,x);
    }
    int q;//no. of query
    scanf("%d",&q);
    while(q--)
    {
        int x,y;// segment x<=y
        scanf("%d %d",&x,&y);
        printf("%d\n",query(1,1,n,x,y));
    }

    return 0;
}













