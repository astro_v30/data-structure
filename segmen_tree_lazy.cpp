#include<bits/stdc++.h>
//astro_lion
using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())
#define pii pair
#define MP make_pair

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2 scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)

const int N=100005;
int tree[N*4],lazy[N*4];

void init(int node,int st,int ed,int pos,int val)
{
    if(st==ed && st==pos)
    {
        tree[node]+=val;
        return;
    }
    int mid=(st+ed)>>1;
    int left=node*2;
    int right=left+1;
    if( pos<=mid ) init(left,st,mid,pos,val);
    else init(right,mid+1,ed,pos,val);
    tree[node]=max(tree[left],tree[right]);
}

void update(int node,int st,int ed,int i,int j,int val)
{
    int mid=(st+ed)>>1;
    int left=node*2;
    int right=left+1;

    if(lazy[node])
    {
        tree[node]+=lazy[node];
        if(st ^ ed)
        {
            lazy[left]+=lazy[node];
            lazy[right]+=lazy[node];
        }
        lazy[node]=0;
    }

    if(st==i && ed==j)
    {
        tree[node]+=val;
        if(st ^ ed)
        {
            lazy[left]+=val;
            lazy[right]+=val;
        }
        return;
    }

    if(j<=mid )update(left,st,mid,i,j,val);
    else if(i>mid) update(right,mid+1,ed,i,j,val);
    else
    {
        update(left,st,mid,i,mid,val);
        update(right,mid+1,ed,mid+1,j,val);
    }
    tree[node]=max(tree[left],tree[right]);
}

int query(int node,int st,int ed,int i,int j)
{
    int mid=(st+ed)>>1;
    int left=node*2;
    int right=left+1;

    if(lazy[node])
    {
        tree[node]+=lazy[node];
        if(st ^ ed)
        {
            lazy[left]+=lazy[node];
            lazy[right]+=lazy[node];
        }
        lazy[node]=0;
    }

    if(st==i && ed==j)
    {
        return tree[node];
    }

    if(j<=mid) return query(left,st,mid,i,j);
    else if(i>mid) return query(right,mid+1,ed,i,j);
    else
    {
        int ret1=query(left,st,mid,i,mid);
        int ret2=query(right,mid+1,ed,mid+1,j);
        int ret=max(ret1,ret2);
        return ret;
    }

}



int main()
{
    int n;
    read(n);
    fo(i,1,n+1)
    {
        int x;
        read(x);
        init(1,1,n,i,x);
    }
    puts("0 for range update, 1 for range query -1(any) for quit ");
    int check;
    while(true)
    {
        read(check);
        if(check == 0 )
        {
            int x,y,val;
            read2(x,y),read(val);
            update(1,1,n,x,y,val);
        }
        else if(check == 1)
        {
            int x,y;
            read2(x,y);
            prn(query(1,1,n,x,y));
        }
        else
        {
            puts("bye !!");
            break;
        }
    }


    return 0;
}












